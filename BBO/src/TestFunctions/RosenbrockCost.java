package TestFunctions;

import BBO.FitnessFunction;
import BBO.Habitat;

public class RosenbrockCost implements TestFunction, FitnessFunction<Double>
{
	private double fitness = 0.0d; 
	@Override
	public void evaluate(Habitat<Double> habitat) 
	{
		fitness = 0.0d;
		// TODO Auto-generated method stub
		for (int i = 0 ; i < habitat.getSIVs().length -1 ; i++)
		{
			fitness += 	100 * 
						Math.pow((habitat.getSIVs()[i+1] - Math.pow(habitat.getSIVs()[i], 2)), 2) +
						Math.pow((habitat.getSIVs()[i] - 1), 2);
		}
	}

	@Override
	public double getHSI() 
	{
		// TODO Auto-generated method stub
		return fitness;
	}

}
