package TestFunctions;

import BBO.Habitat;

public interface TestFunction 
{
	public void evaluate(Habitat<Double> SIV);
	
	public double getHSI(); 
}
