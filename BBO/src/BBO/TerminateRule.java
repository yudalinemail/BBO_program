package BBO;

public interface TerminateRule 
{
	public boolean isSatisfyRule();
	
	public String getCurrentSituation();
}
