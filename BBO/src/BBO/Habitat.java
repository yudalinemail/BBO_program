package BBO;

/**
 * This interface define the Habitat which consisted of suitability index variables (SIVs).
 * The SIVs means a suitable solution in your problem.
 * 
 * @param	<T>	The encoding category.
 * */
public interface Habitat <T>
{
	public void buildSIV(T[] SIVs);
	
	public T[] getSIVs();
	
	/**
	 * Set the habitat suitability index (HSI).
	 * The HSI is the fitness value in evolutionary algorithm. 
	 * */
	public void setHSI(double value);
	
	public double getHSI();
}
