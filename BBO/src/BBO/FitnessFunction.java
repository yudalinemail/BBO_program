package BBO;

public interface FitnessFunction<T> 
{
	public void evaluate(Habitat<T> SIV);
	
	public double getHSI();
}
