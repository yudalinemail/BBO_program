package BBO;

import java.util.Arrays;
import java.util.Comparator;

public class Habitats<T>
{
	
	Habitat<T>[] habitats = null;
	
	public Habitats(Habitat<T>[] habitats)
	{
		this.habitats = habitats;
	}
	
	public Habitat<T> getIndexOfHabitats(int index)
	{
		return this.habitats[ index ];
	}
	
	public Habitat<T>[] getHabitat()
	{
		return this.habitats;
	}
	
	@SuppressWarnings("unchecked")
	public void sort(@SuppressWarnings("rawtypes") Comparator habitats_Comparator)
	{
		Arrays.sort(habitats, habitats_Comparator);
	}
}
