package BBO;

/**
 * 
 * */
public interface Initialization <T>
{
	public void initializePopulation();
	
	/**
	 * The habitat indicates the population in BBO.
	 * */
	public Habitat<T>[] getHabitat(); 
}
