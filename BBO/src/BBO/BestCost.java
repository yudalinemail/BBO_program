package BBO;

public class BestCost<T> 
{
	Habitat<T> habitat = null;
	
	public BestCost(Habitat<T> habitat)
	{
		this.habitat = habitat;
	}
	
	public void saveBestCost(Habitat<T> habitat)
	{
		this.habitat = habitat;
	}
	
	@SuppressWarnings("rawtypes")
	public Habitat getHabitat()
	{
		return this.habitat;
	}
	
	public double getHSIFromBestCost()
	{
		return this.habitat.getHSI();
	}
}
