package BBO;

public class EliteSolutions<T> 
{
	private Habitat<T>[] elites = null;
	private int numberOfElites = 0;
	
	@SuppressWarnings("unchecked")
	public EliteSolutions(int numberOfElites)
	{
		elites = new Habitat[numberOfElites];
		this.numberOfElites = numberOfElites;
	}
	
	public void saveBestCost(Habitat<T>[] SIV)
	{
		for(int i = 0 ; i < numberOfElites ; i++)
		{
			if (elites[i] != null)
			{
				elites[ i ].buildSIV(SIV[ i ].getSIVs());
				elites[ i ].setHSI(SIV[ i ].getHSI());
			}else{
				elites[ i ] = SIV[ i ];
			}
			
		}
	}
	
	@SuppressWarnings("rawtypes")
	public Habitat[] getElites()
	{
		return elites;
	}
}
