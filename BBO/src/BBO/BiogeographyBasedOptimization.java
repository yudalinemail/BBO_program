package BBO;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

import OptimizationProblem.BasicParameters;
import OptimizationProblem.Objective;


public class BiogeographyBasedOptimization<T> 
{
	
	private Initialization<T> initialization = null;
	private FitnessFunction<T> fitnessFunction = null;
	private Habitats<T> habitats = null;
	private Habitats<T> z = null;
	private static BasicParameters basicParameters = null;
	private static BBO_Parameters bboParameters = null;
	private Objective objective = null;
	private BestCost<T> bestCost = null;
	private EmigrationRates emigrationRates = null;
	private ImmigrationRates immigrationRates = null;
	@SuppressWarnings("rawtypes")
	private EliteSolutions eliteSolutions = null;
	private TerminateRule terminateRule = null;
	
	public BiogeographyBasedOptimization()
	{
		 
	}
	
	public void implement()
	{
		
		preprocessingBBO();
		repeatedComputation();
		
		
	}
	
	private void preprocessingBBO()
	{
		initializeHabitat();
		
		evaluateHSI();
				
		sortPopulation();
				
		saveBestHabitat();
		
		computeMigrationRates();
	}
	
	/**
	 * Initialize the population
	 * */
	private void  initializeHabitat()
	{
		initialization.initializePopulation();
		habitats = new Habitats<T>( initialization.getHabitat() );
		z = habitats;
	}
	
	/**
	 * Compute the cost of each individual
	 * */
	private void evaluateHSI()
	{
		for(int i = 0 ; i < habitats.getHabitat().length ; i++)
		{
			fitnessFunction.evaluate(habitats.getHabitat()[i]);
			habitats.getHabitat()[i].setHSI(fitnessFunction.getHSI());
		}
	}
	
	/**
	 * Sort the population from best to worst
	 * */
	private void sortPopulation()
	{
		habitats.sort(objective.Habitat_Comparator());
	}
		
	@SuppressWarnings("unchecked")
	/**
	 * Save the best cost at each generation
	 * */
	private void saveBestHabitat()
	{
		if(bestCost!=null)
		{
			bestCost.saveBestCost(objective.compare(bestCost.getHabitat(), habitats.getHabitat()[0]));
		}else{
			bestCost = new BestCost<T>(habitats.getHabitat()[0]);
		}
	}
	
	/**
	 * Compute migration rates, assuming the population is sorted 
	 * 		from most fit to least fit.
	 * */
	private void computeMigrationRates()
	{
		computeEmigrationRates();
		computeImmigrationRate();
	}
	
	private void computeEmigrationRates()
	{
		emigrationRates = new EmigrationRates(basicParameters.getPopulationSize());
	}
	
	private void computeImmigrationRate()
	{
		immigrationRates = new ImmigrationRates(emigrationRates);
		System.out.println("Emigration rates: " + Arrays.toString(emigrationRates.getEmigrationRates()));
		System.out.println("Immigration Rates: " + Arrays.toString(immigrationRates.getImmigrationRates()));
	}	
	
	private void repeatedComputation()
	{
		Thread thread = new Thread(){
			public void run()
			{
				while(terminateRule.isSatisfyRule() != true)
				{
					System.out.println(terminateRule.getCurrentSituation());
					for(int i = 0 ; i < habitats.getHabitat().length ; i++)
					{
						System.out.print(Arrays.toString(habitats.getHabitat()[i].getSIVs()) + "\t");
						System.out.println("Fit:" + habitats.getHabitat()[i].getHSI());
					}
					
					System.out.print("Best cost: " + Arrays.toString(bestCost.getHabitat().getSIVs()));
					System.out.println("\tFit: "+bestCost.getHSIFromBestCost());
										
					
					saveEliteSolutions();
					
					decideHowMuchInformationToShareBetweenSolutions();
					
					mutationAndReplace();
					
					evaluateHSI();
					
					sortPopulation();
					
					replaceWorstIndividuals();
					
					sortPopulation();
					
					saveBestHabitat();
				}
			}
		};
		thread.run();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void saveEliteSolutions()
	{
		if (eliteSolutions != null)
		{
			eliteSolutions.saveBestCost(habitats.getHabitat());
		}else{
			eliteSolutions = new EliteSolutions(bboParameters.getNumberOfElites());
			eliteSolutions.saveBestCost(habitats.getHabitat());
		}
	}
	
	/**
	 * Use migration rates to decide how much information to share between solutions
	 * */
	private void decideHowMuchInformationToShareBetweenSolutions()
	{
		
//		System.out.println("*****************decide*********************");
		
		Random rand = new Random(basicParameters.getRandomSeed());
		for(int i = 0 ; i < basicParameters.getPopulationSize() ; i++)
		{
			for(int j = 0 ; j < basicParameters.getProblemDimension() ; j++)
			{
				if(rand.nextDouble() < immigrationRates.getImmigrationRates()[i])
				{
					double randomNum = rand.nextDouble() * emigrationRates.getSum();
					double select = emigrationRates.getEmigrationRates()[0];
					int selectIndex = 0;
					while(( randomNum > select ) && ( selectIndex < basicParameters.getPopulationSize() - 1 ))
					{
						selectIndex = selectIndex +1;
						select = select + emigrationRates.getEmigrationRates()[selectIndex];
					}
					z.getHabitat()[i].getSIVs()[j] = habitats.getHabitat()[selectIndex].getSIVs()[j]; 
				}
				else
				{
					z.getHabitat()[i].getSIVs()[j] = habitats.getHabitat()[i].getSIVs()[j]; 
				}
			}
		}
//		for(int i = 0 ; i < z.getHabitat().length ; i++)
//		{
//			System.out.println(Arrays.toString(z.getHabitat()[i].getVariables()) + "\t");
//		}
	}
	
	/**
	 * Mutation
	 * */
	@SuppressWarnings("unchecked")
	private void mutationAndReplace()
	{
//		System.out.println("*****************Mutation*********************");
		habitats = new Mutation(z, basicParameters).getMutatedHabitat();
//		for(int i = 0 ; i < habitat.getHabitat().length ; i++)
//		{
//			System.out.println(Arrays.toString(habitat.getHabitat()[i].getVariables()) + "\t");
//		}
	}
	
	@SuppressWarnings("unchecked")
	private void replaceWorstIndividuals()
	{
		habitats = new ReplaceWorstByElites(habitats, eliteSolutions).getNewHabitat();
	}
	
	public void setInitialization(Initialization<T> initialization)
	{
		this.initialization = initialization;
	}
	
	public void setFitnessFunction(FitnessFunction<T> function)
	{
		this.fitnessFunction = function;
	}

	@SuppressWarnings("static-access")
	public void setBasicParameters(BasicParameters parameters)
	{
		this.basicParameters = parameters;
	}
	
	@SuppressWarnings("static-access")
	public void setBBOParameters(BBO_Parameters parameters)
	{
		this.bboParameters = parameters;
	}
	
	public void setObjective(Objective objective)
	{
		this.objective = objective;
	}
	
	public void setTerminateRule(TerminateRule terminateRule)
	{
		this.terminateRule = terminateRule;
	}
}
