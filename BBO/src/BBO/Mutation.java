package BBO;

import java.util.Random;

import OptimizationProblem.BasicParameters;

public class Mutation 
{
	@SuppressWarnings("rawtypes")
	private Habitats habitats = null;
	
	private BasicParameters parameters = null;
	
	@SuppressWarnings("rawtypes")
	public Mutation(Habitats habitats, BasicParameters parameters)
	{
		this.habitats = habitats;
		this.parameters = parameters;
	}
	
	@SuppressWarnings("rawtypes")
	public Habitats getMutatedHabitat()
	{
		double mutationProbability = new BBO_Parameters().getMutationProbability();
		Random rand = new Random(parameters.getRandomSeed());
		for(int i = 0 ; i < parameters.getPopulationSize() ; i++)
		{
			for(int j = 0 ; j < parameters.getProblemDimension() ; j++)
			{
				if(rand.nextDouble() < mutationProbability)
				{
					habitats.getHabitat()[i].getSIVs()[j] = parameters.getMinDomain() +
																	(parameters.getMaxDomain() - parameters.getMinDomain()) *
																	rand.nextDouble();
				}
			}
		}
		return habitats;
	}
}
