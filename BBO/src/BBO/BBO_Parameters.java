package BBO;


public class BBO_Parameters implements Parameters 
{
	/**
	 * The mutation probability per solution per independent variable.
	 * */
	private double mutationProbability = 0.04;
	
	/**
	 * 		How many of the best solutions to keep 
	 * from one generation to the next generation.
	 * */
	private int numberOfElites = 2;
		
	public BBO_Parameters()
	{
		
	}
	
	/**
	 * @param	mutationProbability	The mutation probability per solution per independent variable.
	 * @param	numberOfElites	How many of the best solutions to keep from one generation to the next generation.
	 * */
	public BBO_Parameters(	double mutationProbability,
							int numberOfElites)
	{
		this.mutationProbability = mutationProbability;
		this.numberOfElites = numberOfElites;
	}
	
	public void setMutationProbability(double mutationProbability)
	{
		this.mutationProbability = mutationProbability;
	}
	
	public void setNumberOfElites(int numberOfElites)
	{
		this.numberOfElites = numberOfElites;
	}
	
	public double getMutationProbability()
	{
		return this.mutationProbability;
	}
	
	public int getNumberOfElites()
	{
		return this.numberOfElites;
	}
	
}
