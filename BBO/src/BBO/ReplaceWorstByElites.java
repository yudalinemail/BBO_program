package BBO;

public class ReplaceWorstByElites 
{
	@SuppressWarnings("rawtypes")
	private Habitats habitats = null;
	
	@SuppressWarnings("rawtypes")
	EliteSolutions eliteSolutions = null;
	
	@SuppressWarnings("rawtypes")
	public ReplaceWorstByElites(Habitats habitat, EliteSolutions eliteSolutions)
	{
		this.habitats = habitat;
		this.eliteSolutions = eliteSolutions;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Habitats getNewHabitat()
	{
		
		for(int i = 1 ; i <= eliteSolutions.getElites().length ; i++)
		{
//			System.out.println("Elite " + i + " :\t"+Arrays.toString(eliteSolutions.getSIVs()[ i - 1].getVariables()));
			
			habitats.getHabitat()[habitats.getHabitat().length - i].buildSIV(eliteSolutions.getElites()[ i - 1].getSIVs());
			habitats.getHabitat()[habitats.getHabitat().length - i].setHSI(eliteSolutions.getElites()[ i - 1].getHSI());
		}
		return habitats;
	}
}
