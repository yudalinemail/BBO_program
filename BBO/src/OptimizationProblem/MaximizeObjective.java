package OptimizationProblem;

import java.util.Comparator;

import BBO.Habitat;

public class MaximizeObjective implements Objective
{
	@SuppressWarnings("rawtypes")
	private static Comparator<Habitat> habitats_Comparator 
							= new Comparator<Habitat>()
	{

		@Override
		public int compare(	Habitat habitat1,
							Habitat habitat2) 
		{
			// TODO Auto-generated method stub
			return habitat2.getHSI() > habitat1.getHSI() ? 1: habitat2.getHSI() < habitat1.getHSI() ? -1 : 0;
		}
		
	};

	@SuppressWarnings("rawtypes")
	@Override
	public Comparator<Habitat> Habitat_Comparator() 
	{
		// TODO Auto-generated method stub
		return habitats_Comparator;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Habitat compare(Habitat habitat1, Habitat habitat2) 
	{
		// TODO Auto-generated method stub
		
		return habitat1.getHSI() > habitat2.getHSI() ? habitat1 : habitat2;
	}
	
}
