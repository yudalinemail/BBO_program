package OptimizationProblem;

public class Objectives 
{
	public static final Objective maximizeObjective = new MaximizeObjective();
	
	public static final Objective minimizeObjective = new MinimizeObjective();
}
