package OptimizationProblem;

import java.util.Comparator;

import BBO.Habitat;

public interface Objective 
{
	@SuppressWarnings("rawtypes")
	public Comparator<Habitat> Habitat_Comparator();
	
	@SuppressWarnings("rawtypes")
	public Habitat compare(Habitat habitat1, Habitat habitat2);
}
