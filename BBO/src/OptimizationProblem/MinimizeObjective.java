package OptimizationProblem;

import java.util.Comparator;

import BBO.Habitat;

public class MinimizeObjective implements Objective
{
	@SuppressWarnings("rawtypes")
	private static Comparator<Habitat> SIV_Comparator 
							= new Comparator<Habitat>()
	{

		@Override
		public int compare(	Habitat habitat1,
							Habitat habitat2) 
		{
			// TODO Auto-generated method stub
			return habitat1.getHSI() > habitat2.getHSI() ? 1: habitat1.getHSI() < habitat2.getHSI() ? -1 : 0;
		}
		
	};
	
	@SuppressWarnings("rawtypes")
	@Override
	public Comparator<Habitat> Habitat_Comparator() 
	{
		// TODO Auto-generated method stub
		return SIV_Comparator;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Habitat compare(Habitat habitat1,
											Habitat habitat2) 
	{
		// TODO Auto-generated method stub
		
		return habitat1.getHSI() > habitat2.getHSI() ? habitat2 : habitat1;
	}
}
