package OptimizationProblem;

import TestFunction.BasicParametersForTestFunction;
import BBO.TerminateRule;

public class TerminateRuleForMaximumGeneration implements TerminateRule
{
	private int currentGeneration = 0;
	private int maximumGeneration = 0;
	
	public TerminateRuleForMaximumGeneration()
	{
		maximumGeneration = new BasicParametersForTestFunction().getGeneration();
	}
	
	@Override
	public boolean isSatisfyRule() 
	{
		// TODO Auto-generated method stub
		if(currentGeneration != maximumGeneration)
		{
			currentGeneration++;
			return false;
		}else
		{
			currentGeneration = 0;
			return true;
		}
	}

	@Override
	public String getCurrentSituation() 
	{
		// TODO Auto-generated method stub
		return String.valueOf(currentGeneration);
	}
	

}
