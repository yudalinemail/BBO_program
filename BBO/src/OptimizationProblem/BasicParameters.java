package OptimizationProblem;

import BBO.Parameters;

public abstract class BasicParameters implements Parameters
{
	/**
	 * The generation count limit.
	 * */
	public int generation = 0;
	
	/**
	 * The population size.
	 * */
	public int populationSize = 0;
	
	/**
	 * The number of variables in each solution (i.e., problem dimension).
	 * */
	public int problemDimension = 0;
	
	/**
	 * The lower bound of each element of the function domain.
	 * */
	public double minDomain = 0.0d;
	
	/**
	 * The upper bound of each element of the function domain.
	 * */
	public double maxDomain = 0.0d;
	
	/**
	 * The randomSeed.
	 * */
	public int randomSeed = 0;
	
	public BasicParameters()
	{
		
	}
	
	/**
	 * @param	generation	The generation count limit.
	 * @param	populationSize	The population size.
	 * @param	problemDimension	The number of variables in each solution (i.e., problem dimension).
	 * @param	minDomain	The lower bound of each element of the function domain.
	 * @param	maxDomain	The upper bound of each element of the function domain.
	 * */
	public BasicParameters(	int generation,
							int populationSize,
							int problemDimension,
							double minDomain,
							double maxDomain)
	{
		this.generation = generation;
		this.populationSize = populationSize;
		this.problemDimension = problemDimension;
		this.minDomain = minDomain;
		this.maxDomain = maxDomain;
	}
	
	public abstract void setGeneration();
	
	public abstract void setPopulationSize();
	
	public abstract void setProblemDimension();
	
	public abstract void setMinDomain();
	
	public abstract void setMaxDomain();
	
	public abstract void setRandomSeed();
	
	public int getGeneration()
	{
		return this.generation;
	}
	
	public int getPopulationSize()
	{
		return this.populationSize;
	}
	
	public int getProblemDimension()
	{
		return this.problemDimension;
	}
	
	public double getMinDomain()
	{
		return this.minDomain;
	}
	
	public double getMaxDomain()
	{
		return this.maxDomain;
	}
	
	public int getRandomSeed()
	{
		return this.randomSeed;
	}
}
