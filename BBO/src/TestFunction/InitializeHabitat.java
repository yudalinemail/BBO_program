package TestFunction;

import java.util.Random;

import BBO.Initialization;

public class InitializeHabitat implements Initialization<Double>
{

	SIVs[] habitat = null;
	
	BasicParametersForTestFunction parameter = new BasicParametersForTestFunction();
	
	Random rand = new Random(parameter.getRandomSeed());
	
	@Override
	public void initializePopulation() 
	{
		
		habitat = new SIVs[parameter.getPopulationSize()];
		// TODO Auto-generated method stub
		for (int i = 0 ; i < parameter.getPopulationSize() ; i++)
		{
			habitat[ i ] = generateSIV();
		}
		System.out.println(habitat.length);
	}
	
	private SIVs generateSIV()
	{
		Double[] solution = new Double[parameter.getProblemDimension()]; 
		for (int i = 0 ; i < parameter.getProblemDimension() ; i++)
		{
			solution[ i ] = parameter.getMinDomain() + 
							(parameter.getMaxDomain() - parameter.getMinDomain()) *
							rand.nextDouble();
		}
		return new SIVs(solution);
	}

	@Override
	public SIVs[] getHabitat() 
	{
		// TODO Auto-generated method stub
		return habitat;
	}

}
