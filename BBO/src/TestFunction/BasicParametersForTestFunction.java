package TestFunction;

import OptimizationProblem.BasicParameters;

public class BasicParametersForTestFunction extends BasicParameters
{
	public BasicParametersForTestFunction()
	{
		setGeneration();
		setPopulationSize();
		setProblemDimension();
		setMinDomain();
		setMaxDomain();
		setRandomSeed();
	}

	@Override
	public void setGeneration() 
	{
		// TODO Auto-generated method stub
		super.generation = 100;
	}

	@Override
	public void setPopulationSize() 
	{
		// TODO Auto-generated method stub
		super.populationSize = 5;
	}

	@Override
	public void setProblemDimension() 
	{
		// TODO Auto-generated method stub
		super.problemDimension = 2;
	}

	@Override
	public void setMinDomain() 
	{
		// TODO Auto-generated method stub
		super.minDomain = -2.048;
	}

	@Override
	public void setMaxDomain() 
	{
		// TODO Auto-generated method stub
		super.maxDomain = +2.048;
	}

	@Override
	public void setRandomSeed() 
	{
		// TODO Auto-generated method stub
		super.randomSeed = 0;
	}
	 
}
