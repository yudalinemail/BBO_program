package TestFunction;

import BBO.Habitat;

public class SIVs implements Habitat<Double>
{
	
	private Double[] SIV = null;
	private double HSI = 0.0d;
	
	public SIVs(Double[] variables)
	{
		this.SIV = variables.clone();
	}
	
	@Override
	public void buildSIV(Double[] variables) 
	{
		// TODO Auto-generated method stub
		this.SIV = variables.clone();
	}
	
	@Override
	public Double[] getSIVs() 
	{
		// TODO Auto-generated method stub
		return SIV;
	}

	@Override
	public void setHSI(double HSI) 
	{
		// TODO Auto-generated method stub
		this.HSI = HSI;
	}

	@Override
	public double getHSI() 
	{
		// TODO Auto-generated method stub
		return HSI;
	}

}
