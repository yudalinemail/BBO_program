package TestFunction;

import OptimizationProblem.Objective;
import OptimizationProblem.Objectives;
import OptimizationProblem.TerminateRules;
import TestFunctions.TestFunctions;
import BBO.BBO_Parameters;
import BBO.BiogeographyBasedOptimization;
import BBO.FitnessFunction;
import BBO.TerminateRule;

public class Main 
{
	@SuppressWarnings("unchecked")
	FitnessFunction<Double> testFunction = (FitnessFunction<Double>) TestFunctions.rosenbrockCost;
	Objective objective = Objectives.minimizeObjective;
	TerminateRule terminate = TerminateRules.maximumGeneration;
	
	public Main()
	{
		BiogeographyBasedOptimization<Double> BBO = new BiogeographyBasedOptimization<Double>();
		BBO.setBasicParameters(new BasicParametersForTestFunction());
		BBO.setBBOParameters(new BBO_Parameters());
		BBO.setInitialization(new InitializeHabitat());
		BBO.setFitnessFunction(testFunction);
		BBO.setObjective(objective);
		BBO.setTerminateRule(terminate);
		BBO.implement();
	}
	

	public static void main(String[] s)
	{
		new Main();
	}
}
